<?php

namespace App\Repository;

use App\Entity\TvStations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TvStations|null find($id, $lockMode = null, $lockVersion = null)
 * @method TvStations|null findOneBy(array $criteria, array $orderBy = null)
 * @method TvStations[]    findAll()
 * @method TvStations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TvStationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TvStations::class);
    }

    // /**
    //  * @return TvStations[] Returns an array of TvStations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TvStations
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
