<?php

namespace App\DataFixtures;

use App\Entity\Genre;
use App\Entity\Program;
use App\Entity\TvStations;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tvStation = new TvStations();
        $tvStation->setName('Markiza');
        $tvStation->setDescription('Popis tv stanica');
        $tvStation->setStartTime(new \DateTime('00:00'));
        $tvStation->setEndTime(new \DateTime('03:00'));
        $tvStation->setSlug('markiza');
        $tvStation->setCreatedAt(new \DateTime('now'));
        $manager->persist($tvStation);

        $genre = new Genre();
        $genre->setName('Action');
        $genre->setSlug('action');
        $genre->setCreatedAt(new \DateTime('now'));
        $manager->persist($genre);


        for ($i = 0; $i < 28; $i++) {
            $program = new Program();
            $program->setName('Movie '.$i);
            $program->setDate(new \DateTime('now'));
            $program->setTvStation($tvStation);
            if ($i == 0) {
                $program->setTime(new \DateTime('00:00'));
            } elseif ($i >= 1 AND $i < 24 ) {
                $time = $i.':00';
                $program->setTime(new \DateTime($time));
            } elseif ($i == 24) {
                $program->setTime(new \DateTime('00:00'));
            } elseif($i == 25) {
                $program->setTime(new \DateTime('01:00'));
            } elseif($i == 26){
                $program->setTime(new \DateTime('02:00'));
            } elseif($i == 27) {
                $program->setTime(new \DateTime('03:00'));
            }

            $program->addGenre($genre);

            $program->setDescription('Popis'. $i);
            $program->setCreatedAt(new \DateTime('now'));

            $manager->persist($program);
        }
        $manager->flush();



    }
}
