<?php

namespace App\Controller;

use App\Entity\TvStations;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        // get all TV stations and programs
        $program = $this->getDoctrine()->getRepository(TvStations::class)->findAll();

        if (!$program) {
            throw $this->createNotFoundException(
                'No stations in database, please run command for data fixture.'
            );
        }

        return $this->render('homepage/index.html.twig', [
            'program' => $program,
        ]);
    }

    /**
     * Get program by tv station
     * @Route("/tv-detail/{slug}", name="tv-detail")
     * @param TvStations $tvStations
     * @return Response
     */
    public function detail(TvStations $tvStations) {

        return $this->render('homepage/detail.html.twig', [
            'program' => $tvStations,
        ]);

    }
}
