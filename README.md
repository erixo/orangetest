### Install:

create file in project root and setup DATABASE_URL param

`.env.local`


#### _Commands:_

_create database:_

`php bin/console doctrine:database:create`

_install vendors:_

`composer install`

_create tables:_

`php bin/console doctrine:migrations:migrate`

_Data fixture:_

`php bin/console doctrine:fixtures:load`

